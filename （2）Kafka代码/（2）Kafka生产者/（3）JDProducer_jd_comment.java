package cn.inspur.dataimport;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class JDProducer_jd_comment {
    public static void main(String[] args) throws IOException {
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", "nd11:9092,nd12:9092,nd13:9092");
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer producer = new KafkaProducer(kafkaProps);
        BufferedReader br = new BufferedReader(new FileReader("C:/Users/24350/IdeaProjects/e-commerce/dataimport/src/main/java/cn/inspur/spider/file/jd_comment.csv"));
        String line = null;
        int cnt = 0;
        while ((line = br.readLine()) != null) {
            ProducerRecord<String, String> record = new ProducerRecord<String, String>("jd_comment", "key", line);
            cnt++;
            try {
                producer.send(record).get();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println("生产：" + cnt);
        br.close();
    }

}