import mysql.connector
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

mydb = mysql.connector.connect(
    host="cdb-rov6dd1k.bj.tencentcdb.com",
    port="10181",
    user="root",
    passwd="",
    db='JD',
    charset='utf8'
)
mycursor = mydb.cursor()
sql = "SELECT DISTINCT t.Word from" \
      " (SELECT Word,Cnt FROM FenCi WHERE Word not in " \
      "('好','错','手机','非常','其他','京东','华为','可以','特色'," \
      "'小米','特别','支持','荣耀','使','苹果','高','喜欢','感觉'," \
      "'棒','超级','起来','款','问题','较','长','来','着','屏'," \
      "'能','拍','值得','上','适合','而且','麒麟','之前','要'," \
      "'指纹','合适','体验','?','得','天',';','完美','已经'," \
      "'会','太','人','再','价高','觉得','杠杠','完全')	" \
      "ORDER BY Cnt DESC LIMIT 100) t"
mycursor.execute(sql)
myresult = mycursor.fetchall()  # fetchall() 获取所有记录
str = ""
for tmp in myresult:
    for word in tmp:
        str = str + word + " "

fontpath = 'C:/Users/24350/IdeaProjects/e-commerce/dataimport/src/main/java/cn/inspur/spark/AliHYAiHei-Beta.ttf'
imgpath = 'C:/Users/24350/IdeaProjects/e-commerce/dataimport/src/main/java/cn/inspur/spark/langchao.png'
outpath = 'C:/Users/24350/IdeaProjects/e-commerce/dataimport/src/main/java/cn/inspur/spark/'
mask = np.array(Image.open(imgpath))
wordcloud = WordCloud(mask=mask, font_path=fontpath, mode='RGBA', background_color=None).generate(str)
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis('off')
# plt.show()
wordcloud.to_file(outpath + 'inspur.png')
